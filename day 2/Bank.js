class PrivateClass {
	
	
}

export default class Bank {

	constructor(b) {
		this.balance = b
	}
	
	showBal() {
		console.log(`Available Balance ${this.balance}`)
	}

	deposit(amt) {
		this.balance += amt;
	}
	
	withdraw(amt){
		new PrivateClass();
        this.balance -= amt;
    }


}