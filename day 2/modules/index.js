import Bank from './Bank.js';
import SBIBank from './SBIBank.js';


class HDFCBank extends Bank {

}


var acc1 = new SBIBank(1000);
acc1.showBal();

acc1.deposit(500);
acc1.showBal();

acc1.withdraw(300);
acc1.showBal();

/*
hobj1 = new HDFCBank(1000)
hobj1.deposit(500)
hobj1.showBal()
hobj1.withdraw(300)
hobj1.showBal()


hobj2 = new HDFCBank(5000)
hobj2.deposit(500)
hobj2.showBal()
hobj2.withdraw(300)
hobj2.showBal()
*/