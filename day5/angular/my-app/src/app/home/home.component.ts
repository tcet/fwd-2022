import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  fruits = ["apple", "banana", "mango", "chiku"];

  userRecords:any;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  foo() {
    alert("I am from foo fun");
  }

  getUsers() {

    let url = "https://jsonplaceholder.typicode.com/users";
    return this.http.get(url).subscribe((response:any) => {
      console.log(response);
      this.userRecords = response;
    })

  }

}
