/*
https://domainname.com/user
 
Record
Insert	- CREATE	- POST
Read	- READ		- GET
Modify	- UPDATE	- UPDATE
Delete	- DELETE 	- DELETE

*/

var express = require("express");
//you need to install express using following command
//npm i express
var app = express();
app.use(express.json());

//npm i cors
var cors = require('cors');
app.use(cors());

const port = 8989;

app.post("/sub", (req, res) => {
	res.setHeader('Content-Type', 'application/json');
	data = req.body;
	
	var sub = req.body.num1 - req.body.num2;
	
	var result = {"result" : sub }
	res.send(JSON.stringify(result));
	//res.send("sub api");
})

//http://localhost:8989/
app.get("/", (req, res) => {
	res.setHeader('Content-Type', 'text/html');
	res.send("Hello Friends... Chai Pilo");
})

app.get("/add", (req, res) => {
	res.setHeader('Content-Type', 'application/json');
	data = req.body;
	res.send(JSON.stringify(data));
	//res.send("add api");
})


app.put("/mul", (req, res) => {
	res.setHeader('Content-Type', 'text/html');
	res.send("put api");
})

app.delete("/div", (req, res) => {
	res.setHeader('Content-Type', 'text/html');
	res.send("div api");
})


app.listen(port, () => {
	console.log(`Listening at port ${port}`);
})