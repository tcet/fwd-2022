const port = 8080;
const mongoose = require("mongoose");
const conn_str = "mongodb://tcetadmin:tcetadmin@cluster0-shard-00-00.dslyw.mongodb.net:27017,cluster0-shard-00-01.dslyw.mongodb.net:27017,cluster0-shard-00-02.dslyw.mongodb.net:27017/tcet2022?ssl=true&replicaSet=atlas-3xk2hf-shard-0&authSource=admin&retryWrites=true&w=majority";


mongoose.connect(conn_str, { useNewUrlParser: true , useUnifiedTopology: true})
	.then( () => console.log("Connected successfully...") )
	.catch( (err) => console.log(err) );

const userSchema = new mongoose.Schema({
	name: String,
	age: Number,
	city: String
});

const student = new mongoose.model("students", userSchema);

/** Express Mongoose Integration **/
const express = require("express");
var cors = require('cors');
const app = express();

//add middlewares
app.use(express.json());
app.use(cors());

app.route("/student")
.get(async (req, res) => {
	let data = await student.find();
	res.send(data);
})
.post(async (req, res) => {
	req_data = req.body;
	let obj = new student(req_data)
	let result = await obj.save();
	res.send(result);
})


app.listen(process.env.PORT || port, () => {
	console.log(`listening ${port}...`);
});